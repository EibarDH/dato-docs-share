# Models - Base Project.

To ease development of models and aim for standarization, we've created a base project structure within DatoCMS (see figure No. 1).


*Figure No. 1*

<img src="./figures/sample-project-datocms.png">

NOTE: "?" sign means `optional`.

1. **Global config**
      * Model:
          * label: string => Name of record
          * value: string => Value of record
      * Description: used to store values that would be used across several other components, such as: phone numbers, emails, etc. In the future, would store theme settings (colors, fonts...).

2. **Links**
      * Model:
          * displayText1: string => Value to be used as text to click to follow link.
          * displayText2?: string => Extra display value
          * internalHref?: Link => page or post to anchor
          * linkToExternalUrlInstead?: boolean => tells the system to use the external href value instead of any internalHref value set.
          * externalHref?: string => anchor to another website
          * useActionInstead?: boolean => when enabled, `action` field is shown.
          * action?: `call` | `email` | `popup` | `scroll` | `download`=> when the anchor shouldn't link to an URL but execute an action instead.
          * ForceNativeNavigation?: boolean => enforces no-precached navigation (to tell the front NOT to use Gatsby link)
      * Description: used to create Link objects. Should exist at least one pointing to each `Page`.

3. **Pages**
      * Model:
          * name: string => page human-readable identifier (or name).
          * slug: string => slug for this particular page. ie: /contact-us would show on https://domain.com/contact-us.
          * pageMeta?: DatoCmsMeta => Meta tags configuration for the page.
          * Header?: Link<`Header`> => Header component to be used for this page.
          * Sections?: Array<`Link`<`Section`>> => Sections to show on this page. Can select multiple
          * Footer?: Link<`Footer`> => Footer component to be used for this page.
        * Description: used to create pages for the website.

4. **Headers**: Folder to keep `Header` models.

5. **Sections**: Folder to keep `Section` models.

5. **Footer**: Folder to keep `Section` models.

5. **Utils**: Folder to keep utility models.

6. **Button**
      * Model:
          * text?: string => text to be shown within the button.
          * link?: `Link` => `Link` record this button is associated to.
          * justText?: boolean => makes the button show as just text in the front-end.
          * iconButton?: boolean => tells the front-end to render a clickable icon instead of a design system button. if true shows "icon" field.
          * icon?: `Media` => svg asset. Only shown when iconButton is enabled.

You may encounter some other pre-defined Models depending on the project, other common ones are:

* **Template**: to define a set of sections for some pages, like post-category pages for example.
* **Post**: Similar to pages Model, but for posts. Here you create posts in a "wordpress-like" fashion.

Congratulations!. At this point you're ready to create and review some models 🎉, keep it up!.
