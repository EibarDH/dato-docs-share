# Models - fields

If you landed on this doc, then you should have read [Models - names](./Models-names.md) already; if you haven't, then please do so before continuing.

Now that we know how to name pieces of content, it's time to dive deeper into widgets and fields.

## Field

A field is a tool that captures data, is visually represented by a widget that allows content editors to input data from the CMS. A field has a type (may be of type image, text, date, etc..)

Since we'll be using DatoCMS, the following figure shows available fields:

*figure No.1*

<img src="./figures/fields.png">

And you can find the description for each on [this page](https://www.datocms.com/docs/content-modelling).

IF YOU ARE A DEVELOPER: Make sure you read the complete documentation listed on the link above before continuing. It explains everything regarding fields, from types to validation.

Now here's how DatoCMS entity hiearchy is defined: [https://www.datocms.com/docs/general-concepts/data-modelling](https://www.datocms.com/docs/general-concepts/data-modelling).

As you can see, MODELS are composed by FIELDS, and MODELS represent a "template" to create RECORDS that have content on the shape of the MODEL itself; therefore, a MODEL can have many FIELDS, and be used to create many RECORDS. A FIELD  is isolated to his MODEL, has a type a _can_ have validation. A RECORD is a full piece of content composed by more granular content pieces.

# How names and fields come togehter?

Well, when creating a field, after selecting the field type, you'll be prompted to input a name (see figure No. 2), this name must be set following [name conventions](./Models-names.md) as seen on previous lesson.

*figure No.2*

<img src="./figures/field-name-prompt.png">

To be more specific, the `Field ID` must correlate to a name following the [name conventions](./Models-names.md), but as you type a name you'll see the `Field ID` is auto-generated based on the name. Try to make this two values match, but if you consider the `Name` should have extra spaces or words to ease readability, feel free to do so and then make sure the `Field ID` **does** follow the  [name conventions](./Models-names.md). It's important to say that `Field ID` will try to automatically mirror the `Name` value as you type, but replacing spaces for underscores, which is totally fine. Underscores are the separators for words on the `Field ID`.

## Validation and presentation standards

Each field has a set of settings that can be customized to adjust widgets behavior on the CMS, the following list sets up a convention for each field type based on the field names:

1. **title**
    * type: Text - Single-line string
    * name: title
    * validation: none
    * default: none
    * presentation: default


2. **body text**
      * type: Text - Multi-paragraph text
      * name: paragraph
      * validation: none
      * default: none
      * presentation
              * Field editor: HTML editor
              * HTML EDITOR SETTINGS: ALL but "Add image"


3. **button[count]**
      * type: Links - Single link
      * name: button[count]
      * validation: Accept only specified model -> Button
      * presentation: default


4. **image**
      * type: Media - Single asset
      * name: image
      * validation:
              * Accept only specified extensions -> Image
              * Require alt and/or title -> Alternate text
      * presentation: default


5. **label**
      * type: Text - Single-line string
      * name: title
      * validation: none
      * default: none
      * presentation: default


6. **link**
      * type: Links - Single link
      * name: link or [common sense]
      * validation: Accept only specified model -> Link
      * presentation: default


7. **[common sense]**: any


8. **block[count]**
      * type: fieldset (clicking the "Add new fieldset" button).
      * drag and drop other fields inside of it.

### Others

9. **Array of items**
      * type: Modular content
      * name: [common sense]
      * validation: as needed
      * presentation: default
      * other tips:
            * Then add inside any needed fields (which are called "blocks" by DatoCMS).
            * Do not edit existing blocks since they may be shared across components.

[Next guide: Models - Base Project](./Models-base-project.md)