# DatoCMS Models Documentation

# Overview

DatoCMS is a content management system that we've specially configured to administer content for websites tailored to our needs. With it, we associate "Models" (set of fields) to "Sections" (a section of the page) to buildout structured data for the frontend to show.

# Goals of this documentation

1. Set up a roadmap for models creation.

2. Set up a standard for models.

2. Set up a base of knowledge so the way in which the information is entered for each Model (ie: Section) is as uniform as possible.

# Specifications

# Models - Names

In the spirit to keep things consistent over time, this guide summarizes a list of suggestions that will ease the integration process of DatoCMS Sections and the frontend, by providing a standarized way to name section fields.

On our abstraction, all "sections" are "components" and have "models".

## Defining names for most common content pieces

Take as example the section shown below, here we classify each piece of content for the given section under a "category" or "type".

_figure No.1_
<img src="./figures/section-fields-figure1.png"  alt="section-fields-example"/>

**Convention: The name of a content piece, will define the name of the field within the DatoCMS Model.**

Here's a list of most common names for content pieces:

1. **title**: For sections a title represents a headline. Titles are usually bold and bigger text of a section or any other entity. A title is basically anything using an `<h1>` to `<h6>` HTML tag but can be visually identified in the mockups by knowing the design system. In DatoCMS it will be associated with the following Widget, which will refer to the text field and will be explained later but the correct association with such icons is important.

_figure No.2_

<img src="./figures/Text.png"  alt="widget text"/>
<img src="./figures/Single-line-string.png" alt="widget text single line"/>

2. **body text** (called **paragraph** on previous versions): It's basically body text of any component. Can be visually identified for having the characteristic of the body text accorging to the design system. It can contain multiple HTML elements, so it's not limited to just paragraphs. A group of HTML lists followed by paragraphs and common html tags used for text, would be considered a paragraph as a whole, since this will accept HTML markup coming from the CMS. In DatoCMS it will be associated with the following Widget; which will be explained later but the correct association with such icons is important.

_figure No.3_

<img src="./figures/Text.png"  alt="widget text"/>
<img src="./figures/Multiple-paragraph-text.png" alt="widget text multiple line"/>

3. **button[count]**: For buttons and "standalone text anchors" (basically a text with an anchor that's not inserted within a paragraph or a list of links) defined in the design system. It's important to _**always add a numeric count suffix starting at 1**_ in case another button needs to be added in the future. So as shown on figure No. 1, if a section has two buttons, one would be tagged "button1" and "button2" respectively. In DatoCMS it will be associated with the following Widget; which will be explained later but the correct association with such icons is important.

_figure No.4_

<img src="./figures/Links.png"  alt="widget links"/>
<img src="./figures/Single-link.png" alt="widget single link"/>

4. **image**: Any jpg, svg or png asset. If a component (Section or any other) has two images, then add a numeric count suffix starting at 1. In DatoCMS it will be associated with the following Widget; which will be explained later but the correct association with such icons is important.

_figure No.5_

<img src="./figures/Media.png"  alt="widget mediat"/>
<img src="./figures/Single-asset.png" alt="widget single asset"/>

5. **label**: A section identifier. Used when a component has no copy at all, this field has the purpose to give the CMS user a way to label it, so it can be easily identified. Must add a tooltip with this explanation on the field settings. In DatoCMS it will be associated with the following Widget; which will be explained later but the correct association with such icons is important.

_figure No.6_

<img src="./figures/Text.png"  alt="Widget text"/>
<img src="./figures/Single-line-string.png" alt="widget text single line"/>

6. **link** Used when it's needed to say that a component must link to another place, but there's no a buttons on it. As a matter of fact, the buttons have a link associated behind scenes. So if there's an `<a>` tag wrapped on a bunch of text within your component but it doesn't looks like a button, then there's need for a link. Add a numeric count suffix starting at 1 if needed. In DatoCMS it will be associated with the following widget; which will be explained later but the correct association with such icons is important.

_figure No.7_

<img src="./figures/Links.png"  alt="Widget links"/>
<img src="./figures/Single-link.png" alt="widget single link"/>

Note that button and link use the same icon and note that text and label also.

Another good use for **link** are sets of navigation elements in nav bars or footers.

When you finish entering the information in example 1 you should see the following screen:

_figure No.8_

<img src="./figures/hero.png" alt="widget text multiple line"/>

The procedure to obtain this result is as follows:

1. Go to "settings" tab.
2. Navigate to models panel.
3. Press the "+" symbol and put the name you will assign to the section preceded by the word "Section", for example: "Section Hero 2"z.

_figure No.10_

<img src="./figures/Paso1.png" alt="symbol plus"/>

_figure No.11_

<img src="./figures/Paso2.png" alt="Add model"/>

_figure No.12_

<img src="./figures/Paso3.png" alt="Select Name Model plus"/>

2. We have built the Secccion Hero 2 model

_figure No.13_

<img src="./figures/Paso4.png" alt="final Model"/>

3. Pressing "Add new field" button the following screen is displayed

_figure No.14_

<img src="./figures/Paso5.png" alt="Select widget"/>

4. Let's say we want to add a label to this Section so it can be easily identified, we'd select the "Text" widget to assign it one and choose "single line string" option.

_figure No.15_

<img src="./figures/Paso6.png" alt="Select widget"/>

5.  The following screen will be displayed in which you would assign the Name "Label".

_figure No.16_

<img src="./figures/Paso7.png" alt="Select widget"/>
<img src="./figures/Paso8.png" alt="Select widget"/>

6. at the end of the procedure, your screen will look like this:

_figure No.17_

<img src="./figures/Paso9.png" alt="Select widget"/>

Now you can repeat this procedure to create the other fields for our "Section Hero 2" Model.

## Let's see another example:

Now consider the following figure:

_figure No.18_

<img src="./figures/SectionCards.png" alt="Cards"/>

On this example we catalog pieces of content within a cards listing section, and here's where common sense come to play.

We've named basic pieces of content as explained above, but now we have a inner _sets_ of content. We have here a set of cards, and a set of content pieces for the cards themselves.

It's important to pay attention to the difference between pieces named "cards" and "card". Clearly, **"cards" represents and array of sets** (array of cards), and **"card" instead represents the repeatable set of values for each array item.**

This "array of items" situation defines a need our next name to exist:

7. **[common sense]**: Used to name pieces by what they represent. Use carefully. Suited to name array of items and items within arrays.

at the end of the procedure in DatoCMC, it should look like this ...

_figure No.19_

<img src="./figures/SectionCardsDatoCMS.png" alt="Cards"/>

but within modular content

_figure No.20_

<img src="./figures/CardsInterno.png" alt="Cards"/>

note that although there are several cards, only a single modular content should be created.
Note that the modular contents are named in the plural (Cards) and inside them in the singular (card).

## Let's see another example:

Now consider the following figure:

_figure No.21_

<img src="./figures/section-fields-figure2.png"  alt="section-fields-example"/>

Here, we're listing a set of blog posts. It is particularly important because even though it has modular content it will not be treated that way. "Modular content" widgets are to be used to set up repeatable structures with flexible values, but in this case since all blog posts are going to have a page, it makes sense to have them been its own model, and then link to them on this section.

For this particular model we'll go with the following fields:

1. Title (text-line-string)
2. Links (Multiple-links) querying "Posts" Records

On the other hand, the "Post" Model would have the following fields:
1. Image (Media-single-asset)
2. Title (text-single-line)
3. Slug (Links) associated with another link
4. Publishing date - (Date)
5. Excerpt (text-multiple-paragraph HTML)
6. BodyText (text-multiple-paragraph HTML)
7. PostCategory (Links ) queryinng "Post category" Records.

Now, the "Post Category" Model would be as follow:

1. Title (text-single-line)
2. Link (Links) associated with a "Link" Record (on this case a "Link" record means an anchor to a page or external URL, we'll explain that better later on...)

So then, after building out this complete structure you'd see:

_figure No.22_

<img src="./figures/SectionPost1.png"  alt="section-post-example"/>

which is associated with Post

_figure No.23_

<img src="./figures/Post.png"  alt="section-post-example"/>
<img src="./figures/PostContinued.png"  alt="section-post-example"/>

## Let's see another example:

Now consider the following figure:

_figure No.24_
<img src="./figures/section-fields-figure3.png"  alt="section-fields-example"/>

Here we have a footer with 4 well defined blocks.

On this case the blocks have different content pieces, and we've decided to group them on "field sets", each one of those field sets, are to be named "block"

8. **block[count]**: A group of content pieces, used to visually group _non-repeatable_ set of content pieces. It's important to _**always add a numeric count suffix starting at 1**_ in case a new block needs to be added in the future.

The procedure to build the blocks is as follows:

NOTE: by pressing the "Add New Fieldset" button, you can group fields.

_figure No.25,26,27_

<img src="./figures/AddNewFieldSetButton.png"  alt="button AddNewFieldSet"/>
<img src="./figures/NameBlock.png"  alt="NameBlock"/>
<img src="./figures/BlockVisual.png"  alt="Block creation"/>

With this fieldset, use the procedure already known to create more fields inside.

_figure No.28_

<img src="./figures/BlockVisualArrow.png"  alt="button Arrow"/>

after adding some fields to your fieldset, you should have the following result

_figure No.29,30,31,32_

<img src="./figures/FinalSectionFooter2.png"  alt="SectionFooter2"/>
<img src="./figures/FinalSectionFooter2Cont1.png"  alt="SectionFooter2"/>
<img src="./figures/FinalSectionFooter2Cont2.png"  alt="SectionFooter2"/>
<img src="./figures/FinalSectionFooter2Cont3.png"  alt="SectionFooter2"/>

[Next guide: Models - fields](./Models-fields.md)
